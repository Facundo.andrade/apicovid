<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Muertos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muertos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaas_id');
            $table->integer('numero');

        });
    }

    public function down()
    {
        Schema::dropIfExists('muertos');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
