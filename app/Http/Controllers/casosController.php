<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class casosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {

        $casos = casos::all();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    public function store(Request $request)
    {
        $casos = new casos();
        $casos->fecha = $request->fecha;
        $casos->ccaas_id = $request->ccaas_id;
        $casos->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $casos = casos::where("fecha",$id)->first();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($casos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $casos = casos::where('fecha',$request->fecha)->first();
        $casos ->fecha = $request->fecha;
        $casos ->ccaas_id = $request->ccaas_id;
        $casos ->numero = $request->numero;
        $casos->save();
        return response()->json($casos);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $casos = casos::where('fecha',$id)->first();
        if ($casos){
            $casos ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);

    }

    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $casos = DB::select(DB::raw("select * from casos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($casos);

    }
}
