<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\muertos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class muertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {
        $muertos = muertos::all();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    public function store(Request $request)
    {
        $muertos = new muertos();
        $muertos->fecha = $request->fecha;
        $muertos->ccaas_id = $request->ccaas_id;
        $muertos->numero = $request->numero;
        $muertos->save();
        return response()->json($muertos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $muertos = muertos::where("fecha",$id)->first();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($muertos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $muertos = muertos::where('fecha',$request->fecha)->first();
        $muertos ->fecha = $request->fecha;
        $muertos ->ccaas_id = $request->ccaas_id;
        $muertos ->numero = $request->numero;
        $muertos->save();
        return response()->json($muertos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $muertos = muertos::where('fecha',$id)->first();
        if ($muertos){
            $muertos ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $muertos = DB::select(DB::raw("select * from muertos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($muertos);

    }
}
