<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {

        $ia7 = Ia7::all();
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia7],200);
    }

    public function store(Request $request)
    {
        $ia7 = new Ia7();
        $ia7->fecha = $request->fecha;
        $ia7->ccaas_id = $request->ccaas_id;
        $ia7->incidencias = $request->incidencias;
        $ia7->save();
        return response()->json($ia7);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ia7 = ia7::where("fecha",$id)->first();
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($ia7);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ia7 = ia7::where('fecha',$request->fecha)->first();
        $ia7 ->fecha = $request->fecha;
        $ia7 ->ccaas_id = $request->ccaas_id;
        $ia7 ->incidencia = $request->incidencia;
        $ia7->save();
        return response()->json($ia7);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $ia7 = ia7::where('fecha', $id)->first();
        if ($ia7) {
            $ia7->delete();
        } else {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No hay nada'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $ia7 = DB::select(DB::raw("select * from ia7 where fecha BETWEEN '$id1' and '$id2' "));

        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($ia7);

    }
}
