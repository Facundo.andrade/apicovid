<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\CCAAs;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')
            ->first();

        $ccaa = ccaas::where('id', $this->ccaas_id)->first();

        if($this->numero != null){

            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->numero,
            ];


        }else if($this->incidencia != null){
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->incidencia,

            ];

        }


    }
}
