<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\CCAAs;
use Illuminate\Support\Facades\DB;

class ShowResourceNum extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pais = DB::table('paises')->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')->select('paises.*')->first();
        $ccaa = ccaas::where('id', $this->ccaas_id)->first();

        return [
            'fecha' => $this->fecha,
            'ccaa' => $ccaa->nombre,
            'pais' => $pais->nombre,
            'value' => $this->numero,
        ];

    }
}
