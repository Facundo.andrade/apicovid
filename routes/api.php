<?php

use App\Http\Controllers\casosController;
use App\Http\Controllers\Ia14Controller;
use App\Http\Controllers\Ia7Controller;
use App\Http\Controllers\muertosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('ia14', Ia14Controller::class);
Route::post('ia14/{fecha}', [Ia14Controller::class,'show']);
Route::get('/ia14all', [Ia14Controller::class,'showAll']);
Route::post('/ia14', [Ia14Controller::class,'store']);
Route::post('ia14update',[Ia14Controller::class,'update']);
Route::post('ia14delete/{fecha}',[Ia14Controller::class,'delete']);
Route::get('/ia14/{fecha}/{fecha2}', [Ia14Controller::class,'showCollection']);

Route::resource('ia7', Ia7Controller::class);
Route::post('ia7/{fecha}', [Ia7Controller::class,'show']);
Route::get('/ia7all', [Ia7Controller::class,'showAll']);
Route::post('/ia7', [Ia7Controller::class,'store']);
Route::post('ia7update',[Ia7Controller::class,'update']);
Route::post('ia7delete/{fecha}',[Ia7Controller::class,'delete']);
Route::get('/ia7/{fecha}/{fecha2}', [Ia7Controller::class,'showCollection']);


Route::resource('casos', casosController::class);
Route::post('casos/{fecha}', [casosController::class,'show']);
Route::get('/casosall', [casosController::class,'showAll']);
Route::post('/casos', [casosController::class,'store']);
Route::post('casosupdate',[casosController::class,'update']);
Route::post('casosdelete/{fecha}',[casosController::class,'delete']);
Route::get('/casos/{fecha}/{fecha2}', [casosController::class,'showCollection']);



Route::resource('muertos', muertosController::class);
Route::post('muertos/{fecha}', [muertosController::class,'show']);
Route::get('/muertosall', [muertosController::class,'showAll']);
Route::post('/muertos', [muertosController::class,'store']);
Route::post('muertosupdate',[muertosController::class,'update']);
Route::post('muertosdelete/{fecha}',[muertosController::class,'delete']);
Route::get('/muertos/{fecha}/{fecha2}', [muertosController::class,'showCollection']);

